package com.scom.scom5gdistributor;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.scom.scom5gdistributor.Receiver.ConnectivityReceiver;

public class Scom5GDistributor extends Application {

    private static Scom5GDistributor mInstance;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public static synchronized Scom5GDistributor getInstance() {
        return mInstance;


    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


}

