package com.scom.scom5gdistributor.SearchNumber;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.scom.scom5gdistributor.DefineData;
import com.scom.scom5gdistributor.HomePage.HomeFragment;
import com.scom.scom5gdistributor.Model.DividerItemDecoration;
import com.scom.scom5gdistributor.Model.HTTPURLConnection;
import com.scom.scom5gdistributor.Model.Item;
import com.scom.scom5gdistributor.NavigationDrawer.HomeActivity;
import com.scom.scom5gdistributor.R;
import com.scom.scom5gdistributor.Receiver.ConnectivityReceiver;
import com.scom.scom5gdistributor.Scom5GDistributor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SearchNumberFragment extends android.support.v4.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SearchNumberAdapter mAdapter;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    String token;
    SharedPreferences sharedpreferences;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_img_bg,rel_no_records,rel_no_internet;
    private FirebaseAnalytics mFirebaseAnalytics;
    EditText edt_search_number;
    ImageView img_search;
    public SearchNumberFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView= inflater.inflate(R.layout.fragment_search_number, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);

        edt_search_number=(EditText)rootView.findViewById(R.id.edt_search_number);
        img_search=(ImageView)rootView.findViewById(R.id.img_search);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Scom5GDistributor.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_recharge_history);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        rel_img_bg= (RelativeLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_records= (RelativeLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) rootView.findViewById(R.id.rel_no_internet);

        txt_title.setText("Search Recharges");

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);


        mAdapter = new SearchNumberAdapter(movieList,getActivity(),"Search Number");
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query=edt_search_number.getText().toString();
                boolean isError=false;

                if(null==query||query.length()==0||query.length()<7)
                {
                    isError=true;
                    edt_search_number.setError("Invalid Mobile Number");
                }

                if(!isError) {
                    if(checkConnection()) {
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new FetchRecharges().execute(query);
                    }else{
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    //display the recharge transactions details
    private class FetchRecharges extends AsyncTask<String, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);


        }
        @Override
        protected Void doInBackground(String... params) {

            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("phoneNumber", params[0]);
                this.response = new JSONObject(service.POST(DefineData.SEARCH_RECHARGE,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("message")) {
                            msg = response.getString("message");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);
                                    String Id = json2.getString("id");
                                    String OperatorId = json2.getString("operator_id");
                                    String OperatorName = json2.getString("operator_name");
                                    String TransactionNo = json2.getString("transactionNo");
                                    String MobileNo = json2.getString("mobileNo");
                                    String Time = json2.getString("time");
                                    String Commission = json2.getString("commission");
                                    String Amount = json2.getString("amount");
                                    String Status = json2.getString("status");
                                    String RetailerName = json2.getString("retailerName");
                                    superHero = new Item(MobileNo, "₹ " + Amount, TransactionNo, Commission, Id, OperatorName, Time, Status,
                                            OperatorId,RetailerName);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing response");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    rel_img_bg.setVisibility(View.GONE);
                                    progress_linear.setVisibility(View.GONE);

                                }

                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("No Records Founds");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing response");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        edt_search_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_search_number.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        Scom5GDistributor.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);

    }
    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

}
